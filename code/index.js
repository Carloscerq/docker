const express = require("express");
const fs = require("fs");
require("dotenv").config()

const app = express();
app.use(express.json());

app.post("/", (req, res) => {
  const { company } = req.body;
  fs.appendFileSync("./data.txt", company);
  res.send(`${company} was added to ${process.env.FILE}`);
});

app.get("/", (req, res) => {
  try {
    const data = fs.readFileSync("./data.txt", "utf8");
    res.send(data);
  } catch (err) {
    console.log(err);
  }
})

app.listen(8080, () => {
  console.log("Server is running");
});
