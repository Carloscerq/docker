  const express = require("express");
const dotenv = require("dotenv");
const mysql = require("mysql2");

const app = express();
app.use(express.json());
dotenv.config();

const db = mysql.createConnection({
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
});

db.connect((err) => {
  if (err) throw err;
  var sql =
    "CREATE TABLE company (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255))";
  db.query(sql, function (err, result) {
    if (err) console.log("Table already exists");
    console.log("Table created");
  });
});

app.get("/", (req, res) => {
  db.connect(function (err) {
    if (err) throw err;
    db.query("SELECT * FROM company", function (err, result, fields) {
      if (err) throw err;
      console.log(result);
      res.send(result);
    });
  });
});

app.post("/", (req, res) => {
  const { company } = req.body;

  db.connect(function (err) {
    if (err) throw err;
    console.log("Connected!");
    var sql = `INSERT INTO company (name) VALUES ('${company}')`;
    db.query(sql, function (err, result) {
      if (err) throw err;
      console.log("1 record inserted");
    });
  });

  res.send("ok");
});

app.listen(process.env.PORT, () => {
  console.log(`Server is running on port ${process.env.PORT}`);
});
